-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 26, 2016 at 06:37 PM
-- Server version: 5.6.30-0ubuntu0.14.04.1
-- PHP Version: 5.6.22-1+donate.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Udex_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `created_at`, `updated_at`, `name`) VALUES
(1, 'admin@udex.com', '$2y$10$MSwQtCYlhqi37S/MrU3PqOpa6YP18XM8KQyqHsnpwqdWMc2W14Bc.', NULL, NULL, ''),
(2, 'admin@udex.com', '$2y$10$yjSGWr3quHmxwMC8xQTkj.BjbJsqT40dmoR2y4p5wPqXfBbdy0u9a', NULL, NULL, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_21_152946_create_abc_table', 1),
('2016_07_21_153526_add_email_to_abc_table', 1),
('2016_07_26_152422_create_admin_table', 2),
('2016_07_26_155259_add_name_to_admin_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rating_feedback`
--

CREATE TABLE IF NOT EXISTS `rating_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rating_value` int(11) NOT NULL,
  `feedback` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rating_feedback`
--

INSERT INTO `rating_feedback` (`id`, `rating_value`, `feedback`, `created_at`, `updated_at`) VALUES
(1, 1, 'Poor', NULL, '2016-07-22 10:30:50'),
(2, 2, 'Average', NULL, '2016-07-22 10:30:50'),
(3, 3, 'Good', NULL, '2016-07-22 10:31:17'),
(4, 4, 'Very Good', NULL, '2016-07-22 10:31:17'),
(5, 5, 'Excellent', NULL, '2016-07-22 10:36:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_corporate_profile`
--

CREATE TABLE IF NOT EXISTS `t_corporate_profile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userID` int(11) unsigned DEFAULT NULL COMMENT 'foriegn_key',
  `user_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyID` int(11) DEFAULT NULL COMMENT 'candidate_key',
  `company_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `userID_2` (`userID`),
  KEY `userID_3` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `t_corporate_profile`
--

INSERT INTO `t_corporate_profile` (`id`, `company_name`, `userID`, `user_email`, `companyID`, `company_url`, `created_at`, `updated_at`) VALUES
(14, 'indianic', 38, 'test@indianic.com', 0, 'indianic.com', '2016-07-04 22:16:51', '2016-07-20 05:14:56'),
(15, 'indianic', 42, 'kundan.roy@indianic.com', 14, 'indianic.com', '2016-07-04 22:19:58', '2016-07-20 05:15:04'),
(16, 'mailinator', 43, 'kundan@indianic.com', 0, 'indianic.com', '2016-07-04 22:21:30', '2016-07-20 05:16:34'),
(17, 'mailinator', 44, 'kundan2@mailinator.com', 16, 'mailinator.com', '2016-07-05 21:55:14', '2016-07-20 05:15:24'),
(18, 'mailinator', 45, 'etest@mailinator.com', 16, 'mailinator.com', '2016-07-12 23:58:30', '2016-07-18 04:30:05'),
(25, 'Admin', 66, 'admin@admin.com', 16, 'admin.com', '2016-07-13 00:11:02', '2016-07-21 06:31:03'),
(26, 'mailinator', 65, 'kroy@mailinator.com', 16, 'mailinator.com', '2016-07-13 00:11:53', '2016-07-20 05:05:04'),
(28, 'mailinator', 67, 'shiv@mailinator.com', 16, 'mailinator.com', '2016-07-13 02:12:57', '2016-07-20 05:05:25'),
(29, 'mailinator', 68, 'etest11@mailinator.com', 16, 'mailinator.com', '2016-07-13 02:19:04', '2016-07-20 05:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_group`
--

CREATE TABLE IF NOT EXISTS `t_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `default_page` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `GroupID` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_interview`
--

CREATE TABLE IF NOT EXISTS `t_interview` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `condidate_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interviewerID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `criteriaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interview_create_by` int(11) unsigned DEFAULT NULL,
  `interview_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `rating_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `rating` float(10,1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `t_interview`
--

INSERT INTO `t_interview` (`id`, `condidate_name`, `interviewerID`, `short_description`, `criteriaID`, `interview_create_by`, `interview_date`, `comment`, `rating_status`, `rating`, `created_at`, `updated_at`) VALUES
(1, 'vikram', '43,42', 'MCA Ajmer', '1,2,7,10', 1, '', '', 'Pending', 0.0, '2016-07-01 07:04:39', '2016-07-25 11:25:37'),
(2, 'kunal', '43,44', 'MCA,DAVV,Indore', '1,2,7,10', 1, '', '', 'Pending', 0.0, '2016-07-01 07:14:06', '2016-07-14 07:43:49'),
(3, 'kundan3', '44', 'BE ahmedabad', '1,2', 1, '', NULL, 'Pending', 0.0, '2016-07-04 02:18:28', '2016-07-12 12:40:16'),
(9, 'ankit', '42', 'MBA in HR', '1', NULL, '', NULL, 'Pending', 0.0, '2016-07-12 07:06:29', '2016-07-23 10:48:15'),
(10, 'shiv', '42', 'MBA in HR', '1', NULL, '', NULL, 'Pending', 0.0, '2016-07-12 07:13:19', '2016-07-23 10:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `t_interview_criteria`
--

CREATE TABLE IF NOT EXISTS `t_interview_criteria` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `interview_criteria` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `t_interview_criteria`
--

INSERT INTO `t_interview_criteria` (`id`, `interview_criteria`, `created_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'Attitude', 0, '0000-00-00 00:00:00', NULL, '2016-07-18 01:23:50'),
(2, 'CS', 0, '0000-00-00 00:00:00', NULL, '2016-07-15 04:38:38'),
(3, 'Personality', 0, '0000-00-00 00:00:00', NULL, '2016-07-15 04:38:38'),
(4, 'technicalSkill', 2, '2016-07-01 03:51:52', NULL, '2016-07-15 04:38:38'),
(5, 'technicalSkill2', 2, '2016-07-01 03:53:25', NULL, '2016-07-15 04:38:38'),
(6, 'technicalSkill3', 2, '2016-07-01 03:54:15', NULL, '2016-07-15 04:38:38'),
(7, 'CA', 43, '2016-07-05 04:52:29', NULL, '2016-07-15 04:38:38'),
(8, 'technicalSkill1', 43, '2016-07-05 04:58:11', NULL, '2016-07-15 04:38:38'),
(9, 'technicalSkill', 44, '2016-07-05 05:11:16', NULL, '2016-07-15 04:38:38'),
(10, 'technicalSkill2', 42, '2016-07-05 05:12:46', NULL, '2016-07-15 04:38:38'),
(11, 'CEO', 44, '2016-07-05 05:40:56', NULL, '2016-07-15 04:38:38'),
(12, 'test', NULL, '2016-07-18 01:27:09', NULL, '2016-07-18 01:27:09'),
(13, 'test2', NULL, '2016-07-18 01:28:25', NULL, '2016-07-18 01:28:25');

-- --------------------------------------------------------

--
-- Table structure for table `t_interview_rating`
--

CREATE TABLE IF NOT EXISTS `t_interview_rating` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `condidateID` int(11) unsigned DEFAULT NULL COMMENT 'id from t_interview',
  `interviewerID` int(11) unsigned DEFAULT NULL COMMENT 'intervewerID from t_user',
  `interview_criteriaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'interview_criteriaID from  	t_interview_criteria',
  `rating_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` float(10,1) DEFAULT NULL,
  `rating_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_interview_rating`
--

INSERT INTO `t_interview_rating` (`id`, `condidateID`, `interviewerID`, `interview_criteriaID`, `rating_value`, `rating`, `rating_status`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 43, '1,2,3,7,10', '1,2,2,3,4', 2.4, 'Evaluated', 'good', '0000-00-00 00:00:00', '2016-07-25 11:31:18'),
(5, 1, 42, '1,2,3,7,10', '1,2,3,3,4', 2.6, 'Evaluated', 'may be hire', '2016-07-23 07:54:50', '2016-07-26 07:14:16');

-- --------------------------------------------------------

--
-- Table structure for table `t_login_history`
--

CREATE TABLE IF NOT EXISTS `t_login_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) unsigned DEFAULT NULL,
  `device_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_position`
--

CREATE TABLE IF NOT EXISTS `t_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `corporate_group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `t_position`
--

INSERT INTO `t_position` (`id`, `position_name`, `email`, `corporate_group_name`, `company_url`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'Priciple', 'kroy.iips@indianic.com', 'indianic', 'indianic.com', '2016-07-01 09:39:40', NULL, '2016-07-15 02:50:16'),
(2, 'Analyst', 'kundan.roy@indianic.com', 'indianic', 'indianic.com', '2016-07-01 09:41:56', NULL, '2016-07-15 02:50:16'),
(3, 'Managing Director', '', '', NULL, '2016-07-01 09:42:07', NULL, '2016-07-15 02:50:16'),
(4, 'Associates', '', '', NULL, '2016-06-30 10:11:02', NULL, '2016-07-15 02:50:16'),
(6, 'Developer', 'test@mailinator.com', 'mailinator', 'mailinator.com', '2016-06-30 10:10:53', NULL, '2016-07-15 02:50:16'),
(9, 'abc', 'kundan@mailinator.co.net', 'mailinator', 'mailinator.co.net', '2016-07-05 06:27:58', NULL, '2016-07-15 02:50:16'),
(10, 'Director', 'kundan.roy@indianic.com', NULL, NULL, '2016-07-14 07:41:55', NULL, '2016-07-15 02:52:48'),
(11, 'abc22', 'kundan.roy@indianic.com', NULL, NULL, '2016-07-14 07:48:05', '2016-07-15 02:50:55', '2016-07-15 02:50:55'),
(12, 'ere', 'kundan.roy@indianic.com', NULL, NULL, '2016-07-15 02:53:00', '2016-07-15 02:53:09', '2016-07-15 02:53:09'),
(13, 'test123', 'admin@admin.com', NULL, NULL, '2016-07-18 01:44:42', '2016-07-18 01:44:46', '2016-07-18 01:44:46');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password_salt` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `temp_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deviceID` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `device_token` text CHARACTER SET latin1,
  `remember_token` text CHARACTER SET latin1,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `positionID` int(11) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`),
  FULLTEXT KEY `index_name` (`first_name`),
  FULLTEXT KEY `full_text` (`last_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=69 ;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`userID`, `email`, `first_name`, `last_name`, `phone`, `mobile`, `password`, `password_salt`, `temp_password`, `deviceID`, `device_token`, `remember_token`, `last_login_at`, `positionID`, `status`, `created_at`, `updated_at`) VALUES
(38, 'test@indianic.com', ' kunnu', 'kumar', '', '', '$2y$10$M5FI73v.mpCadzkyuYFbf.WSSTY2U8WWL2m4UyU3BgFbmbjWKez6a', '', '', NULL, '', '', NULL, 1, 1, '2016-07-05 00:45:54', '2016-07-15 06:19:42'),
(42, 'kundan.roy@indianic.com', 'kundan', 'roy', '', '', '$2y$10$ll/edjuYwFMClckwmIhHFeVek.jki3fOvEx23jngig9subbtP6vr6', '', '', '', '', 'ag7wy3mXxjzdT3gXEXaUXk2dEaivXTsv5Nsmh8unhJogqDN8HXE6DRcU91CI', NULL, 1, 1, '2016-07-05 03:46:51', '2016-07-25 06:30:36'),
(43, 'kundan@indianic.com', 'kunal', 'roy', '', '', '$2y$10$CZX7lE.hOqaRE.vtAD6Nretz74kJ8vX03qkpKTSozNecOze03IEZu', '', '', '', '', 'AhkkkZeJb75Uobx7ksLKr4XuBjzOaHZX9hfEvhgL7Sm5Soo4PkUgpHmW1rBm', NULL, 4, 1, '2016-07-05 03:49:58', '2016-07-20 06:19:08'),
(44, 'kundan2@mailinator.com', 'John', 'roy', '', '', '$2y$10$ntF4botUHPB/JRTshFZ8Y.C32TUYU8TzREcNaPVj1EXSXqvtNjWEO', '', '', NULL, '', 'iARmMPkUdzAQQotGvLMJCVDsTSnuHsQ5jQm77Scp7QWsxmx4UsHcsPzV9Qj7', NULL, 4, 1, '2016-07-05 03:51:30', '2016-07-15 02:04:38'),
(45, 'etest@mailinator.com', 'kunal', 'roy', NULL, NULL, '$2y$10$39/fs9bmFv.GwK2QQZObE.M0Ugg6mjs1.bR/zLNVVMobGjoMD3Sa2', NULL, NULL, NULL, NULL, NULL, NULL, 6, 0, '2016-07-06 03:25:14', '2016-07-25 06:33:25'),
(65, 'kroy@mailinator.com', 'kundan', 'roy', NULL, NULL, '$2y$10$yZDBkdYaKuVaXvkCBMdRQOmyC1YRQDFOnS2UDuKW4GE3AIMdbs6OW', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2016-07-15 05:06:43', '2016-07-15 05:06:43'),
(66, 'admin@admin.com', 'shivpal', 'dfds', NULL, NULL, '$2y$10$qNzniXpBGj3EQix9uvR3O.wtqaA9R4mDbqrM2GLpj0vDqOXrZHjtC', NULL, NULL, NULL, NULL, 'ZbdmY4J89zLBDKERQRs1ddviO87qw0Rm3LFd1KtpND09aG1ZEPJH725zYwnP', NULL, 1, 0, '2016-07-15 05:26:22', '2016-07-17 23:22:21'),
(67, 'shiv@mailinator.com', 'shivpal', 'singh', NULL, NULL, '$2y$10$dAwD3lDl07LGnm1BzqCoo.4CyH32pwOyQPAboLY7Cj1As4m.YDHZa', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2016-07-15 05:31:44', '2016-07-15 05:31:44'),
(68, 'etest11@mailinator.com', 'shivpal', 'ere', NULL, NULL, '$2y$10$AxMcVUmqovo4gYtCL9sVr.b9zNfyIgq6hOCINnVvq7h1dpiYh76yW', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2016-07-18 03:56:18', '2016-07-18 03:56:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
